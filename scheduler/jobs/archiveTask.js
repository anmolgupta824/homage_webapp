const constantclass = require("../../Constants");
const MongoClient = require('mongodb').MongoClient;

module.exports = function (agenda) {
    agenda.define('archive-task', function (job, done) {
        // Connect to the db
        MongoClient.connect("mongodb://localhost:27017/MASTERDB", {useNewUrlParser: true}).then(function (client) {
            var db = client.db("MASTERDB");
            var collection = db.collection("TASKS");
            collection.findOneAndUpdate({taskid: job.attrs.data.taskid},
                {$set: {status: constantclass.TASKEXPIRED}}, function (err) {
                    if (!err) {
                        done();
                    }
                    console.log(err);
                });
        });
    });
};