const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const logger = logs.getLog();

// Connection URL
const url = 'mongodb://127.0.0.1:27017/';

// Database Name
const dbName = 'MASTERDB';


function insertOneDocument(collectionName, document) {

    return MongoClient.connect(url, {useNewUrlParser: true}).then(function (client) {
        db = client.db(dbName);
        var collection = db.collection(collectionName);

        return collection.insertOne(document);
    }).then(function (items) {
        logger.info("Data inserted successfully! for collection " + collectionName + " :Records Inserted " + items);
        return items;
    });

}


function findoneandReplace(collectionName, query, updatequery) {

    return MongoClient.connect(url, {useNewUrlParser: true}).then(function (client) {
        db = client.db(dbName);
        var collection = db.collection(collectionName);
        return collection.findOneAndReplace(query, updatequery);
    }).then(function (items) {
        logger.info("Data updated successfully! for collection: " + collectionName + " Records Updated " + items.length);
        return items;
    });

}

function findDocumentbyID(collectionName, query) {

    return MongoClient.connect(url, {useNewUrlParser: true}).then(function (client) {
        db = client.db(dbName);
        var collection = db.collection(collectionName);
        return collection.find(query).toArray();
    }).then(function (items) {
        logger.info("Data retrieved successfully! for collection: " + collectionName + " :Records Found " + items.length);
        return items;
    });

}


function findAllDocuments(collectionName) {

    return MongoClient.connect(url, {useNewUrlParser: true}).then(function (client) {
        db = client.db(dbName);
        var collection = db.collection(collectionName);

        return collection.find().toArray();
    }).then(function (items) {
        logger.info("Data retrieved successfully! for collection: " + collectionName + " :Records Found " + items.length);
        return items;
    });

}


function deleteOneDocument(collectionName, document) {

    return MongoClient.connect(url, {useNewUrlParser: true}).then(function (client) {
        db = client.db(dbName);
        var collection = db.collection(collectionName);

        return collection.deleteOne(document);
    }).then(function (items) {
        logger.info("Data deleted successfully! for collection: " + collectionName + " :Records Deleted " + items.length);
        return items;
    });

}

module.exports = {
    insertOneDocument: insertOneDocument,
    findAllDocuments: findAllDocuments,
    findDocumentbyID: findDocumentbyID,
    findoneandReplace: findoneandReplace,
    deleteOneDocument: deleteOneDocument
};