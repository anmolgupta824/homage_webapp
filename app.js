var express = require('express');
var app = express();
logs = require('./WinstonLogs/winston.js');
var logger = logs.getLog();
mongo = require('./Dataaccess/mongoAccess')
constantclass = require('./Constants')
VISIT = require('./Visit/visit.js')
USER = require('./User.js')
kmeans = require('node-kmeans')
var Distance = require('geo-distance');


var bodyParser = require('body-parser')
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// ULTIMATE CHALLENGE :

//Matching algorithm to match  careowner ( careowner has care recepeint) to caregiver
//We will use KMeans Algo to divide the Singapore location into 10 clusters 
//This endpoint will send all the caregivers in the careowner cluster
app.post('/matchVisit', function (req, res) {

    if (req.body == null || req.body.careownerid == null)
        return res.status(400).json({'message' : 'Error'});
    else {
        var careownerid = req.body.careownerid;
        var careownerdata;
        var careownerindex=-1;

        var careGivers;
        var careOwnersStartIndex;
        var careOwners;
        mongo.findAllDocuments("CAREGIVER").then(function (items) {
            careGivers = items;
            careOwnersStartIndex = careGivers.length;

            mongo.findAllDocuments("CAREOWNER").then(function (items) {
                careOwners = items;

                let vectors = new Array();
                // Find all the Caregiver and Careowner in the DB and add location details of each in the vector
                for (let i = 0 ; i < careGivers.length ; i++) {
                    vectors[i] = [ careGivers[i]['location']['lat'] , careGivers[i]['location']['lon']];
                }
                for (let i = 0 ; i < careOwners.length ; i++) {
                    vectors[i + careOwnersStartIndex] = [ careOwners[i]['location']['lat'] , careOwners[i]['location']['lon']];
                    if (careOwners[i]['careownerid'] == careownerid) {
                        careownerindex = i + careOwnersStartIndex;
                        careownerdata = careOwners[i];
                    }
                }
                // Apply KMean Algorithm on Caregiver and Careowner location to clsuter into 10 clusters.
                // For reference purpose we are only taking Singapore locations

                kmeans.clusterize(vectors, {k: 10}, (err, result) => {
                    if (err) {
                        return res.status(400).json({'message' : 'Error while clustering'});
                    }
                    else {

                        if(careownerindex > -1){
                        // Find the owner cluster
                        var ownerCluster = result.filter(value =>  {
                            return value.clusterInd.includes(careownerindex)
                        });

                        var response = Object.assign({}, ownerCluster[0]);
                        response['caregivers'] = [];

                        var caregiver_count=0;

                        // Notify and send all the request to the caregiver in this cluster
                        for(i = 0; i < ownerCluster[0].clusterInd.length; i++) {
                            if (ownerCluster[0].clusterInd[i] < careOwnersStartIndex) {
                                var cg = careGivers[ownerCluster[0].clusterInd[i]];
                                response['caregivers'].push(cg);
                                logger.info("Caregiver " + cg.caregiverid + "found for care owner id " + careownerid);
                                caregiver_count++;
                            }
                        }

                        //If there are no caregiver in the current cluster then we find the nearest cluster
                        // After Finding the nearest cluster we will notify all the caregiver in that cluster
                        if(caregiver_count == 0) {
                            result.sort(function(a, b) {
                                var aloc = {
                                    lat: a.centroid[0],
                                    lon: a.centroid[1]
                                };
                                var bloc = {
                                    lat: b.centroid[0],
                                    lon: b.centroid[1]
                                };
                                return Distance.between(careownerdata.location, aloc) - Distance.between(careownerdata.location, bloc);
                            });

                            var found = 0;
                            for(i = 1; i < result.length && found == 0; i++) {
                                var clust = result[i];
                                for(j = 0; j < clust.clusterInd.length; j++) {
                                    if (clust.clusterInd[i] < careOwnersStartIndex) {
                                        found = 1;
                                        var cg = careGivers[clust.clusterInd[j]];
                                        response['caregivers'].push(cg);
                                        logger.info("Caregiver " + cg.caregiverid + "found for care owner id " + careownerid);
                                    }
                                }
                            }
                        }

                        return res.status(200).json(response);
                    }
                    else{
                        return res.status(400).json({'message' : 'No cluster found for the careownerid'+careownerid+'or the careownerid is invalid'});
                    }

                }
                });

            }, function (err) {
                logger.error('The promise was rejected' + err + err.stack);
                res.send(USER.errorstatusCode("Error fetching Careowners!"));
            });

        }, function (err) {
            logger.error('The promise was rejected' + err + err.stack);
            res.send(USER.errorstatusCode("Error fetching Caregivers!"));
        });
    }
});

// PRO+ CHALLENGE:

// This end point is for Pro+ challenge , assuming we have already matched the caregiver with careowner
app.post('/createVisitRandom', function (req, res) {

  
  if (req.body != null) {
      var visits = VISIT.createVisit(req);
      mongo.insertOneDocument("VISITS", visits).then(function (items) {
          res.send(VISIT.successstatusCode(visits, 'The Visit was created with visitid:' + visits.visitid));
      }, function (err) {
          logger.error('The promise was rejected' + err + err.stack);
          res.send(VISIT.errorstatusCode("Error creating Visit!"));
      });
  } else {
      res.send(VISIT.errorstatusCode("Invalid Request or Invalid parameters!"));
  }

});


// This is for nurse to clock in time , only works after visit is created
app.post('/clockinTime', function (req, res) {

  if (req.body != null && req.body.visitid != null) {

    var visits = {
        visitid: req.body.visitid
    };

    mongo.findDocumentbyID("VISITS", visits).then(function (items) {

        if (items != null && items[0] != null) {

            console.log(items[0]);
            if (items[0].status.match(constantclass.BOOKED)) {
                        
                items[0].status = constantclass.INPROGRESS;
                items[0].clockintime= Date.now();
                var udpatedvisit = items[0];

                mongo.findoneandReplace("VISITS", visits, udpatedvisit).then(function (items) {
                    res.send(VISIT.successstatusCode(udpatedvisit, "Clock in Time was updated for Visit with " + visits.visitid + " Sending Notification to the careowner"+ udpatedvisit.careownerid));
                }, function (err) {
                    logger.error('The promise was rejected' + err + err.stack);
                    res.send(VISIT.errorstatusCode("Error Updating Visit! for visitid " + visits.visitid));
                });
            
            }else {
                res.send(VISIT.errorstatusCode("Visit is not Booked yet " + visits.visitid));
            }


        } else {
            res.send(VISIT.errorstatusCode("Visit Does not exist! for visitid " + visits.visitid));
        }

    }, function (err) {
        logger.error('The promise was rejected' + err + err.stack);
        res.send(VISIT.errorstatusCode("Error Fetching Visit! for visitid " + visits.visitid));
    });

}
else {
    res.send(VISIT.errorstatusCode("visitid missing or Invalid Visit!"));
}

});

// This is for nurse to submit visit summary, only works after nurse is clocked in
app.post('/submitVisitSummary', function (req, res) {

    if (req.body != null && req.body.visitid != null) {
  
      var visits = {
          visitid: req.body.visitid
      };
  
      mongo.findDocumentbyID("VISITS", visits).then(function (items) {
  
          if (items != null && items[0] != null) {
  
              if (items[0].status.match(constantclass.INPROGRESS)) {
                          
                  
                  items[0].visit_summary.heartrate=req.body.heartrate;
                  items[0].visit_summary.pressure=req.body.pressure;
                  items[0].visit_summary.temprature=req.body.temprature;
                  items[0].visit_summary.description=req.body.description;
                  items[0].status = constantclass.SUBMITTED;
                  var udpatedvisit = items[0];
  
                  mongo.findoneandReplace("VISITS", visits, udpatedvisit).then(function (items) {
                      res.send(VISIT.successstatusCode(udpatedvisit, "Visit summary was updated for Visit with " + visits.visitid + " Sending Notification to the careowner"+ udpatedvisit.careownerid));
                  }, function (err) {
                      logger.error('The promise was rejected' + err + err.stack);
                      res.send(VISIT.errorstatusCode("Error Updating Visit! for visitid " + visits.visitid));
                  });
              
              }else {
                res.send(VISIT.errorstatusCode("Invalid Visit request  " + visits.visitid));
            }
          } else {
              res.send(VISIT.errorstatusCode("Visit Does not exist! for visitid " + visits.visitid));
          }
  
      }, function (err) {
          logger.error('The promise was rejected' + err + err.stack);
          res.send(VISIT.errorstatusCode("Error Fetching Visit! for visitid " + visits.visitid));
      });
  
  }
  else {
      res.send(VISIT.errorstatusCode("visitid missing or Invalid Visit!"));
  }
  
});

// This is for nurse to clock out time , only works after nurse records visit summary
app.post('/clockoutTime', function (req, res) {

    if (req.body != null && req.body.visitid != null) {
  
      var visits = {
          visitid: req.body.visitid
      };
  
      mongo.findDocumentbyID("VISITS", visits).then(function (items) {
  
          if (items != null && items[0] != null) {
  
              if (items[0].status.match(constantclass.SUBMITTED)) {
                          
                  items[0].status = constantclass.COMPLETED;
                  items[0].clockouttime= Date.now();
                  var udpatedvisit = items[0];
  
                  mongo.findoneandReplace("VISITS", visits, udpatedvisit).then(function (items) {
                      res.send(VISIT.successstatusCode(udpatedvisit, "Clock Out Time was updated for Visit with " + visits.visitid + " Sending Notification to the careowner"+ udpatedvisit.careownerid));
                  }, function (err) {
                      logger.error('The promise was rejected' + err + err.stack);
                      res.send(VISIT.errorstatusCode("Error Updating Visit! for visitid " + visits.visitid));
                  });
              
              }else {
                res.send(VISIT.errorstatusCode("Invalid Visit request  " + visits.visitid));
            }
          } else {
              res.send(VISIT.errorstatusCode("Visit Does not exist! for visitid " + visits.visitid));
          }
  
      }, function (err) {
          logger.error('The promise was rejected' + err + err.stack);
          res.send(VISIT.errorstatusCode("Error Fetching Visit! for visitid " + visits.visitid));
      });
  
  }
  else {
      res.send(VISIT.errorstatusCode("visitid missing or Invalid Visit!"));
  }
  
  });

  // this is for testing purpose only
  app.post('/bookVisit', function (req, res) {

    if (req.body != null && req.body.visitid != null) {
  
      var visits = {
          visitid: req.body.visitid
      };
  
      mongo.findDocumentbyID("VISITS", visits).then(function (items) {
  
          if (items != null && items[0] != null) {
  
              if (items[0].status.match(constantclass.INITIATED)) {
                          
                  items[0].status = constantclass.BOOKED;
                  var udpatedvisit = items[0];
  
                  mongo.findoneandReplace("VISITS", visits, udpatedvisit).then(function (items) {
                      res.send(VISIT.successstatusCode(udpatedvisit, "Visit was booked " + visits.visitid + " Sending Notification to the careowner"+ udpatedvisit.careownerid));
                  }, function (err) {
                      logger.error('The promise was rejected' + err + err.stack);
                      res.send(VISIT.errorstatusCode("Error Updating Visit! for visitid " + visits.visitid));
                  });
              
              }else {
                res.send(VISIT.errorstatusCode("Invalid Visit request  " + visits.visitid));
            }
          } else {
              res.send(VISIT.errorstatusCode("Visit Does not exist! for visitid " + visits.visitid));
          }
  
      }, function (err) {
          logger.error('The promise was rejected' + err + err.stack);
          res.send(VISIT.errorstatusCode("Error Fetching Visit! for visitid " + visits.visitid));
      });
  
  }
  else {
      res.send(VISIT.errorstatusCode("visitid missing or Invalid Visit!"));
  }
  
  });

// This api will create Caregiver
app.post('/createCaregiver', function (req, res) {

    if (req.body != null) {
        var caregiver = USER.Caregiver(req);
        mongo.insertOneDocument("CAREGIVER", caregiver).then(function (items) {
            res.send(USER.successstatusCode(caregiver, 'The caregiver was created with caregiverid:' + caregiver.caregiverid));
        }, function (err) {
            logger.error('The promise was rejected' + err + err.stack);
            res.send(USER.errorstatusCode("Error creating Caregiver!"));
        });
    } else {
        res.send(USER.errorstatusCode("Invalid Request or Invalid parameters!"));
    }

});

// this api endpoint will get all the caregiver
app.get('/getCareGivers', function (req, res) {

    if (req.body != null) {
        mongo.findAllDocuments("CAREGIVER").then(function (items) {
            res.send(USER.successstatusCode(items, 'Success'));
        }, function (err) {
            logger.error('The promise was rejected' + err + err.stack);
            res.send(USER.errorstatusCode("Error fetching Caregivers!"));
        });
    } else {
        res.send(USER.errorstatusCode("Invalid Request or Invalid parameters!"));
    }

});

// this api endpoint will create careowner
app.post('/createCareOwner', function (req, res) {

    if (req.body != null) {
        var Careowner = USER.Careowner(req);
        mongo.insertOneDocument("CAREOWNER", Careowner).then(function (items) {
            res.send(USER.successstatusCode(Careowner, 'The Careowner was created with Careownerid:' + Careowner.Careownerid));
        }, function (err) {
            logger.error('The promise was rejected' + err + err.stack);
            res.send(USER.errorstatusCode("Error creating Careowner!"));
        });
    } else {
        res.send(USER.errorstatusCode("Invalid Request or Invalid parameters!"));
    }

});

// this api endpoint will get all the careowner
app.get('/getCareOwners', function (req, res) {

    if (req.body != null) {
        mongo.findAllDocuments("CAREOWNER").then(function (items) {
            res.send(USER.successstatusCode(items, 'Success'));
        }, function (err) {
            logger.error('The promise was rejected' + err + err.stack);
            res.send(USER.errorstatusCode("Error fetching Careowners!"));
        });
    } else {
        res.send(USER.errorstatusCode("Invalid Request or Invalid parameters!"));
    }

});

// add carereciver to careowner
app.post('/addCareReceiver', function (req, res) {

    if (req.body != null && req.body.careownerid != null) {
  
        var Careowner = {
            careownerid: req.body.careownerid
        };
    
        mongo.findDocumentbyID("CAREOWNER", Careowner).then(function (items) {
    
            if (items != null && items[0] != null) {
    
                var Carereceipent = USER.addCarereceipent(req);
                 items[0].care_receipients.push(Carereceipent);
                 updatedCareowner=items[0];

                 mongo.findoneandReplace("CAREOWNER", Careowner, updatedCareowner).then(function (items) {
                    res.send(USER.successstatusCode(updatedCareowner, "Carerecepinet was added to Careowner" + Careowner.careownerid+ " Sending Notification to the careowner"+ updatedCareowner.careownerid));
                }, function (err) {
                    logger.error('The promise was rejected' + err + err.stack);
                    res.send(USER.errorstatusCode("Error Updating Careoener! for careownerid " + Careowner.careownerid));
                });
            }
            else {
                res.send(USER.errorstatusCode("Careowner Does not exist! for Careownerid " + Careowner.careownerid));
            }
    
        }, function (err) {
            logger.error('The promise was rejected' + err + err.stack);
            res.send(VISIT.errorstatusCode("Error Fetching Visit! for visitid " + Careowner.careownerid));
        });
    }
    else {
        res.send(VISIT.errorstatusCode("Careownerid missing or Invalid Careowner!"));
    }
});


app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.listen(3000, () => logger.info('homage super-server listening on port 3000!'))
