'use strict';
const winston = require('winston');
const tracer = require('winston-tracer');
const fs = require('fs');
const env = process.env.NODE_ENV || 'development';
const logDir = './WinstonLogs/dailylogs';
const errorlogDir = './WinstonLogs/dailyerrorlogs';
// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}
if (!fs.existsSync(errorlogDir)) {
    fs.mkdirSync(errorlogDir);
}


const tsFormat = () => (new Date());

const {loggers, format, transports} = require('winston');
const {combine, timestamp, label, printf} = format;

const myFormat = printf(info => {
    return `${info.timestamp} [${info.label}] ${info.level}: ${info.message}`;
});


function getLog() {


    const logger = winston.createLogger({
        format: combine(
            label({label: ""}),
            timestamp(),
            myFormat
        ),
        transports: [
            // colorize the output to the console
            new (winston.transports.Console)({
                timestamp: tsFormat,
                colorize: true,
                level: 'info',
                handleExceptions: true
            }),
            new (require('winston-daily-rotate-file'))({
                filename: `${logDir}/homage.log`,
                timestamp: timestamp(),
                datePattern: 'YYYY-MM-DD',
                prepend: true,
                level: env === 'development' ? 'verbose' : 'info'
            }),
            new (require('winston-daily-rotate-file'))({
                filename: `${errorlogDir}/homageError.log`,
                timestamp: timestamp(),
                datePattern: 'YYYY-MM-DD',
                prepend: true,
                level: 'error'
            }),
        ],
        exitOnError: false
    });
    return logger;

}

module.exports = {

    getLog: getLog
}

