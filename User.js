const uuidV1 = require('uuid/v1');
var logger = logs.getLog();

function successstatusCode(users, message) {
    var status = {
        users: users,
        statusCode: 200,
        reasonMessage: message
    }
    logger.info(constantclass.USERCLASS + message);
    return status;
}

function errorstatusCode(message) {
    var status = {
        statusCode: 500,
        reasonMessage: message
    }
    logger.error(constantclass.USERCLASS + message);
    return status;
}

function Caregiver(req) {

    var caregiver = {
        
        caregiverid: uuidV1(),
        name:null,
        location:{
            lat: req.body.lat,
            lon: req.body.lon
        },
        createdtime: Date.now(),
        
    };
    return caregiver;

}

function Careowner(req) {

    var careowner = {
        
        careownerid: uuidV1(),
        name:null,
        location:{
            lat: req.body.lat,
            lon: req.body.lon
        },
        care_receipients:[],
        createdtime: Date.now(),
        
    };
    return careowner;

}

function addCarereceipent(req) {

    var care_receipient = {
        
        care_receipientid: uuidV1(),
        name:req.body.name,
        heartrate:req.body.heartrate, 
        pressure:req.body.pressure, 
        temp:req.body.temp, 
        description:req.body.description,
        createdtime: Date.now(),
        
    };
    return care_receipient;

}



module.exports = {
    Caregiver: Caregiver,
    Careowner:Careowner,
    successstatusCode:successstatusCode,
    addCarereceipent:addCarereceipent,
    errorstatusCode:errorstatusCode
};