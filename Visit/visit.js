const uuidV1 = require('uuid/v1');
var logger = logs.getLog();

function successstatusCode(visits, message) {
    var status = {
        visits: visits,
        statusCode: 200,
        reasonMessage: message
    }
    logger.info(constantclass.VISITCLASS + message);
    return status;
}

function errorstatusCode(message) {
    var status = {
        statusCode: 500,
        reasonMessage: message
    }
    logger.error(constantclass.VISITCLASS + message);
    return status;
}

function createVisit(req) {

    var visit = {
        
        visitid: uuidV1(),
        careownerid:req.body.careownerid,
        caregiverid:req.body.caregiverid,
        status: constantclass.BOOKED,
        // care_receiver: [],
        // slot:[],
        // location:[],
        // visit_summary:[],
        visit_summary:{
            heartrate : null, 
            pressure: null, 
            temprature: null, 
            description:null
        },
        clockintime:null,
        clockouttime:null,
        createdtime: Date.now(),
        
    };
    return visit;

}

module.exports = {
    createVisit: createVisit,
    successstatusCode:successstatusCode,
    errorstatusCode:errorstatusCode
};